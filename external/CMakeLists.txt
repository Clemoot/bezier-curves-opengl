cmake_minimum_required(VERSION 3.8)

include(FetchContent)

#######
# GLM #
#######
FetchContent_Declare(
    glm
    URL ${CMAKE_CURRENT_LIST_DIR}/glm-0.9.9.8.zip
)
FetchContent_MakeAvailable(glm)

########
# GLFW #
########
set(BUILD_SHARED_LIBS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
set(GLFW_INSTALL OFF CACHE BOOL "" FORCE)
FetchContent_Declare(
    glfw
    URL ${CMAKE_CURRENT_LIST_DIR}/glfw-3.3.6.zip
)
FetchContent_MakeAvailable(glfw)

########
# GLAD #
########
FetchContent_Declare(
    glad
    URL ${CMAKE_CURRENT_LIST_DIR}/glad.zip
)
FetchContent_GetProperties(glad)
if (NOT glad_POPULATED)
    FetchContent_Populate(glad)
endif()

add_library(glad STATIC ${glad_SOURCE_DIR}/src/glad.c)
target_include_directories(glad PUBLIC ${glad_SOURCE_DIR}/include)
target_link_libraries(glad ${CMAKE_DL_LIBS})
set_target_properties(glad PROPERTIES CXX_CLANG_TIDY "")

##############
# Dear ImGui #
##############
set(IMGUI_FOLDER imgui-docking)
if (NOT EXISTS ${CMAKE_CURRENT_LIST_DIR}/${IMGUI_FOLDER})
message("Extracting Dear ImGui...")
if (UNIX)
    execute_process(COMMAND unzip -qn ${CMAKE_CURRENT_LIST_DIR}/${IMGUI_FOLDER}.zip
        TIMEOUT 20
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
        COMMAND_ERROR_IS_FATAL ANY)
elseif(WIN32)
    execute_process(COMMAND PowerShell -Command "& {Expand-Archive -LiteralPath ${IMGUI_FOLDER}.zip -DestinationPath .}"
        TIMEOUT 20
        WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
        COMMAND_ERROR_IS_FATAL ANY)
endif()
message("Dear ImGui extracted")
else()
message("Dear ImGui already extracted")
endif()
add_library(imgui STATIC
    ${IMGUI_FOLDER}/imgui.cpp ${IMGUI_FOLDER}/imgui_draw.cpp ${IMGUI_FOLDER}/imgui_tables.cpp
    ${IMGUI_FOLDER}/imgui_widgets.cpp ${IMGUI_FOLDER}/backends/imgui_impl_glfw.cpp
    ${IMGUI_FOLDER}/backends/imgui_impl_opengl3.cpp)
target_compile_definitions(imgui PRIVATE IMGUI_IMPL_OPENGL_LOADER_GLAD)
target_include_directories(imgui PUBLIC ${CMAKE_CURRENT_LIST_DIR} ${CMAKE_CURRENT_LIST_DIR}/${IMGUI_FOLDER})
target_link_libraries(imgui PRIVATE glad glfw)