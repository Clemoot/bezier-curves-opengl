#include <utils/Logging.h>
#include <window/GLFWWindow.h>

ENABLE_GLFW_ERROR_LOG_FUNC;

uint8_t GLFWWindow::smNbGLFWWindows      = 0;
bool    GLFWWindow::smContextInitialized = false;

void GLFWWindow::__initializeContext()
{
    if (smContextInitialized) return;
    glfwSetErrorCallback(logGLFWError);
    glfwInit();

    smContextInitialized = true;
}

GLFWWindow::GLFWWindow(const std::string& name, const uint16_t& width, const uint16_t& height): Window(name, width, height)
{
    __initializeContext();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    mHandle = static_cast<void*>(glfwCreateWindow(width, height, name.c_str(), nullptr, nullptr));
    if (!mHandle) {
        LOG("failed to create glfw window");
        return;
    }

    glfwMakeContextCurrent(static_cast<GLFWwindow*>(mHandle));

    smNbGLFWWindows++;
}

GLFWWindow::~GLFWWindow()
{
    if (mHandle) glfwDestroyWindow(static_cast<GLFWwindow*>(mHandle));
    smNbGLFWWindows--;
    if (smNbGLFWWindows <= 0) {
        glfwTerminate();
        smContextInitialized = false;
    }
}

const bool GLFWWindow::isClosed() const { return glfwWindowShouldClose(static_cast<GLFWwindow*>(mHandle)); }

void GLFWWindow::hide() { glfwHideWindow(static_cast<GLFWwindow*>(mHandle)); }

void GLFWWindow::pollEvents() const { glfwPollEvents(); }

void GLFWWindow::show() { glfwShowWindow(static_cast<GLFWwindow*>(mHandle)); }

inline void GLFWWindow::swapBuffers() const { glfwSwapBuffers(static_cast<GLFWwindow*>(mHandle)); }