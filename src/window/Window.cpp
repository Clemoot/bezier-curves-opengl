#include <window/Window.h>

Window::Window(const std::string& name, const uint16_t& width, const uint16_t& height):
    mHandle(nullptr), mName(name), mWidth(width), mHeight(height), mIsClosed(true)
{
}

Window::~Window() {}

const std::string& Window::name() const { return mName; }

void* const& Window::handle() const { return mHandle; }

const uint16_t& Window::height() const { return mHeight; }

const uint16_t& Window::width() const { return mWidth; }