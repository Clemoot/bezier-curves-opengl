#include <ui/UIWindow.h>

UIWindow::UIWindow(const std::string& windowName, const ImGuiWindowFlags& flags):
    mOpened(true), mWindowFlags(flags), mWindowName(windowName)
{
}

UIWindow::~UIWindow() {}

void UIWindow::displayWindow()
{
    if (mOpened) {
        ImGui::Begin(mWindowName.c_str(), &mOpened, mWindowFlags);
        __displayWindowContent();
        ImGui::End();
    }
}