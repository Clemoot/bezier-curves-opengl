#include <IntTypes.h>

#include <imgui_internal.h>
#include <ui/Dockspace.h>

constexpr const ImGuiWindowFlags dockspaceWindowFlags =
    // ImGuiWindowFlags_MenuBar |
    ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

unsigned int Dockspace::smNbDockspaces = 0;

std::string getDockspaceName(unsigned int id)
{
    constexpr size_t WINDOW_NAME_MAX_LENGTH = 16;
    char windowName[WINDOW_NAME_MAX_LENGTH];
    memset(windowName, 0, WINDOW_NAME_MAX_LENGTH);
    sprintf(windowName, "Dockspace %u", id);
    return std::string(windowName);
}

Dockspace::Dockspace(): UIWindow(getDockspaceName(++smNbDockspaces), dockspaceWindowFlags), mDockspaceID() {}

Dockspace::~Dockspace() {}

void Dockspace::addWindow(std::shared_ptr<UIWindow> window) { mChildWindows.push_back(window); }

void Dockspace::displayWindow()
{
    ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(viewport->Pos);
    ImGui::SetNextWindowSize(viewport->Size);
    ImGui::SetNextWindowViewport(viewport->ID);

    ImGui::Begin(mWindowName.c_str(), &mOpened, mWindowFlags);
    mDockspaceID = ImGui::GetID(mWindowName.c_str());
    ImGui::DockSpace(mDockspaceID, ImVec2(0.f, 0.f), ImGuiDockNodeFlags_NoCloseButton);
    ImGui::End();
    for (const std::shared_ptr<UIWindow>& window : mChildWindows)
        window->displayWindow();
}

void Dockspace::__displayWindowContent() {}
