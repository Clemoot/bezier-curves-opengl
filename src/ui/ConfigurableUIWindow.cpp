#include <ui/ConfigurableUIWindow.h>

ConfigurableUIWindow::ConfigurableUIWindow(const std::string& windowName, const ImGuiWindowFlags& flags):
    UIWindow(windowName, flags)
{
}

ConfigurableUIWindow::~ConfigurableUIWindow() {}