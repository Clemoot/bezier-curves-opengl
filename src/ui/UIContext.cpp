#include <imgui-docking/backends/imgui_impl_glfw.h>
#include <imgui-docking/backends/imgui_impl_opengl3.h>
#include <imgui-docking/imgui.h>
#include <imgui-docking/imgui_internal.h>
#include <ui/UIContext.h>

UIContext::UIContext(Window& window, bool enableDockspace)
{
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    if (enableDockspace) ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_DockingEnable;

    ImGui_ImplGlfw_InitForOpenGL(reinterpret_cast<GLFWwindow* const>(window.handle()), true);
    ImGui_ImplOpenGL3_Init("#version 460");
    ImGui::StyleColorsDark();
}

UIContext::~UIContext()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

void UIContext::startFrame() const
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void UIContext::renderFrame() const
{
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}