#include <IntTypes.h>

#include <imgui_internal.h>
#include <ui/ConfigUIWindow.h>

unsigned int ConfigUIWindow::smNbConfigUIWindows = 0;

std::string getConfigWindowName(unsigned int id)
{
    constexpr size_t WINDOW_NAME_MAX_LENGTH = 20;
    char windowName[WINDOW_NAME_MAX_LENGTH];
    memset(windowName, 0, WINDOW_NAME_MAX_LENGTH);
    sprintf(windowName, "ConfigUIWindow %u", id);
    return std::string(windowName);
}

ConfigUIWindow::ConfigUIWindow(): UIWindow(getConfigWindowName(++smNbConfigUIWindows)) {}

ConfigUIWindow::~ConfigUIWindow() {}

void ConfigUIWindow::addConfigurableWindow(std::shared_ptr<ConfigurableUIWindow> window)
{
    mConfigurableWindows.push_back(window);
}

void ConfigUIWindow::__displayWindowContent()
{
    for (const std::shared_ptr<ConfigurableUIWindow>& window : mConfigurableWindows)
        window->displayOptions();
}
