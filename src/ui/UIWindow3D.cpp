#include <array>

#include <IntTypes.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <ui/UIWindow3D.h>

unsigned int UIWindow3D::smNbUIWindow3Ds = 0;

std::string get3DWindowName(unsigned int id)
{
    constexpr size_t WINDOW_NAME_MAX_LENGTH = 16;
    char windowName[WINDOW_NAME_MAX_LENGTH];
    memset(windowName, 0, WINDOW_NAME_MAX_LENGTH);
    sprintf(windowName, "UIWindow3D %u", id);
    return std::string(windowName);
}

UIWindow3D::UIWindow3D():
    ConfigurableUIWindow(get3DWindowName(++smNbUIWindow3Ds)),
    mPan(0.f),
    mFramebuffer(),
    mAnchors({ { -1.f, 0.f }, { 1.f, 0.f } }),
    mPolynomials(mAnchors.size() - 1),
    mSampler(mPolynomials)
{
    mSampler.inputMin(0.f).inputMax(1.f).sampleCount(30);
    __computeLine();

    mFramebuffer.createColorAttachment(ColorAttachment::Attachment0);
    mFramebuffer.createDepthStencilAttachment();

    mLineMaterial.lineWidth(10.f);
    mLineMaterial.blendFactor(0.1f);
}

UIWindow3D::~UIWindow3D() {}

void UIWindow3D::displayOptions()
{
    if (ImGui::Button("Show 3D Window"))
        mOpened = true;

    bool recomputeLine = false;

    ImGui::Separator();
    {
        size_t sampleCount    = mSampler.sampleCount();
        size_t minSampleCount = 2;
        if (ImGui::DragScalar("Sample count", ImGuiDataType_U32, &sampleCount, 1, &minSampleCount, nullptr)) {
            mSampler.sampleCount(sampleCount);
            recomputeLine = true;
        }
    }

    ImGui::Dummy({ 0.f, 5.f });

    ImGui::Text("Points :");

    std::string label = "";
    for (unsigned int i = 0, end = mAnchors.size(); i < end; i++) {
        if (ImGui::Button(("X##" + std::to_string(i)).c_str()) && mAnchors.size() > 1) {
            mAnchors.erase(mAnchors.begin() + i);
            i--;
            end--;
            recomputeLine = true;
            continue;
        }
        ImGui::SameLine();
        label = "Point n°" + std::to_string(i + 1);
        recomputeLine |= ImGui::SliderFloat2(label.c_str(), glm::value_ptr(mAnchors[i]), -1.f, 1.f);
    }

    if (ImGui::Button("Add new point")) {
        mAnchors.emplace_back(0.f, 0.f);
        recomputeLine = true;
    }

    if (recomputeLine)
        __computeLine();
}

void UIWindow3D::__displayWindowContent()
{
    ImVec2 windowSize = ImGui::GetContentRegionAvail();

    unsigned int w = static_cast<unsigned int>(windowSize.x);
    unsigned int h = static_cast<unsigned int>(windowSize.y);

    mFramebuffer.attachment(Attachment0)->resize(w, h);
    mFramebuffer.depthAttachment()->resize(w, h);

    mFramebuffer.bind();
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, w, h);
    mLineMaterial.use();
    float aspect   = static_cast<float>(w) / static_cast<float>(h);
    glm::mat4 proj = glm::ortho(-aspect, aspect, -1.0f, 1.0f, -1.0f, 1.0f);
    mLineMaterial.setMVP(proj);
    mLineMaterial.setViewport(w, h);
    mLine.draw();
    assert(glGetError() == GL_NO_ERROR);
    mFramebuffer.unbind();

    auto texture = mFramebuffer.attachment(Attachment0);
    ImGui::Image(
        (void*)(intptr_t)texture->id(), ImVec2(static_cast<float>(texture->width()), static_cast<float>(texture->height())));
}

void UIWindow3D::__computeLine()
{
    mPolynomials.setDegree(mAnchors.size() - 1);
    auto samples = mSampler.sample();

    std::vector<glm::vec2> points;
    points.reserve(samples.size());
    for (decltype(mSampler)::PairType& sample : samples) {
        const std::vector<float>& values = sample.second;
        glm::vec2 point{ 0.f, 0.f };
        for (size_t i = 0; i < mAnchors.size(); i++)
            point += values[i] * mAnchors[i];
        points.push_back(point);
    }

    mLine.points(points);
}