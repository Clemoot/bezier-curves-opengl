#include <array>

#include <App.h>
#include <bezier/Sampler.h>
#include <glm/glm.hpp>

App::App():
    mConfigWindow(std::make_shared<ConfigUIWindow>()),
    mDockspace(std::make_unique<Dockspace>()),
    mGraphicsContext(),
    mUIContext(mWindow, true),
    mWindow("Test window", 1920, 1080),
    m3DWindow(std::make_shared<UIWindow3D>())
{
    mDockspace->addWindow(mConfigWindow);
    mDockspace->addWindow(m3DWindow);

    mConfigWindow->addConfigurableWindow(m3DWindow);
}

App::~App() {}

void App::run()
{
    mWindow.show();

    while (!mWindow.isClosed()) {
        mWindow.pollEvents();

        glClearColor(0.f, 0.f, 0.f, 0.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        mUIContext.startFrame();

        mDockspace->displayWindow();

        mUIContext.renderFrame();

        mWindow.swapBuffers();
    }
}