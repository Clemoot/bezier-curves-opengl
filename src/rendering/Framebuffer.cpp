#include <IntTypes.h>

#include <rendering/Framebuffer.h>
#include <utils/Logging.h>

Framebuffer::Framebuffer(): mDepthStencilAttachment(nullptr)
{
    glGenFramebuffers(1, &mFramebufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, mFramebufferID);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

Framebuffer::~Framebuffer() { glDeleteFramebuffers(1, &mFramebufferID); }

void Framebuffer::bind() const { glBindFramebuffer(GL_FRAMEBUFFER, mFramebufferID); }

void Framebuffer::createColorAttachment(ColorAttachment attachment)
{
    bind();
    mAttachments.emplace_back(TextureAttachment{ std::make_shared<Texture>(GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE),
                                                 GL_COLOR_ATTACHMENT0 + static_cast<unsigned int>(attachment) });
    unbind();
}

void Framebuffer::createDepthStencilAttachment()
{
    mDepthStencilAttachment = std::make_shared<Texture>(GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8);
    bind();
    TextureAttachment{ mDepthStencilAttachment, GL_DEPTH_STENCIL_ATTACHMENT };
    unbind();
}

const bool Framebuffer::isValid() const
{
    bind();

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    unbind();

    return status == GL_FRAMEBUFFER_COMPLETE;
}

void Framebuffer::unbind() { glBindFramebuffer(GL_FRAMEBUFFER, 0); }

const std::shared_ptr<Texture>& Framebuffer::attachment(ColorAttachment attachment) const
{
    return mAttachments[attachment].texture;
}

const std::shared_ptr<Texture>& Framebuffer::depthAttachment() const { return mDepthStencilAttachment; }

const GLuint& Framebuffer::id() const { return mFramebufferID; }

Framebuffer::TextureAttachment::TextureAttachment(std::shared_ptr<Texture> texture, unsigned int attachment):
    texture(texture), attachment(attachment)
{
    texture->use();
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture->id(), 0);
}