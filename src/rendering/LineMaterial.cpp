#include <IntTypes.h>
#include <iostream>

#include <rendering/LineMaterial.h>

LineMaterial::LineMaterial():
    Material("./resources/shaders/lineVs.glsl", "./resources/shaders/lineFs.glsl", "./resources/shaders/lineGs.glsl"),
    mLineWidthLocation(mShader.getUniformLocation("uLineWidth")),
    mBlendFactorLocation(mShader.getUniformLocation("uBlendFactor")),
    mLineWidth(1.0f)
{
}

LineMaterial::~LineMaterial() {}

float LineMaterial::lineWidth() const { return mLineWidth; }

void LineMaterial::lineWidth(float lineWidth)
{
    if (mLineWidthLocation < 0)
        return;
    mLineWidth = lineWidth;
    Material::use();
    mShader.setUniform(mLineWidthLocation, lineWidth);
}

void LineMaterial::blendFactor(float blendFactor)
{
    if (mBlendFactorLocation < 0)
        return;
    Material::use();
    mShader.setUniform(mBlendFactorLocation, blendFactor);
}
