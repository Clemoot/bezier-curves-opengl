#include <string>
#define GLFW_INCLUDE_NONE
#include "rendering/GraphicsContext.h"
#include "utils/Logging.h"
#include <GLFW/glfw3.h>
#include <glad/glad.h>

void APIENTRY debugCallback(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    std::string fullMessage = "";
    switch (severity) {
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            // fullMessage = "Notification: ";
            // break;
            return;

        case GL_DEBUG_SEVERITY_LOW:
            fullMessage = "Info: ";
            break;

        case GL_DEBUG_SEVERITY_MEDIUM:
            fullMessage = "Warning: ";
            break;

        case GL_DEBUG_SEVERITY_HIGH:
            fullMessage = "Error: ";
            break;
    }

    fullMessage += message;

    LOG(fullMessage.c_str());
}

GraphicsContext::GraphicsContext()
{
    if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
        LOG("failed to load glad");
        return;
    }

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(debugCallback, nullptr);
}

GraphicsContext::~GraphicsContext() {}
