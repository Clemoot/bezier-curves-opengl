#include <fstream>
#include <sstream>

#include <IntTypes.h>

#include <rendering/Shader.h>
#include <utils/Logging.h>
#include <utils/Utils.h>

#ifdef GLM_IMPORTED
    #include <glm/glm.hpp>
#endif

std::string readContent(const std::string& filepath)
{
    std::ifstream fs(filepath);

    if (!fs.is_open()) {
        LOG("failed to open file");
        return "";
    }

    std::stringstream ss;
    ss << fs.rdbuf();

    return ss.str();
}

const unsigned int Shader::shaderTypeToGLType(ShaderType type)
{
    static std::array<unsigned int, ShaderType::NbShaderTypes> conversion = { GL_VERTEX_SHADER,
                                                                              GL_GEOMETRY_SHADER,
                                                                              GL_FRAGMENT_SHADER };
    return conversion[type];
}

Shader::Shader(const std::string& vsFilepath, const std::string& fsFilepath, const std::string& gsFilepath):
    mShaderFiles{ __loadShader(VertexShader, vsFilepath),
                  __loadShader(GeometryShader, gsFilepath),
                  __loadShader(FragmentShader, fsFilepath) },
    mVertexShader(mShaderFiles[VertexShader]),
    mGeometryShader(mShaderFiles[GeometryShader]),
    mFragmentShader(mShaderFiles[FragmentShader]),
    mProgram(glCreateProgram())
{
    if (!mVertexShader || !mFragmentShader)
        return;

    glAttachShader(mProgram, mVertexShader->shaderID);
    if (mGeometryShader)
        glAttachShader(mProgram, mGeometryShader->shaderID);
    glAttachShader(mProgram, mFragmentShader->shaderID);
    glLinkProgram(mProgram);

    GLint success = 0;
    glGetProgramiv(mProgram, GL_LINK_STATUS, &success);
    if (!success) {
        GLint length = 0;
        glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &length);
        std::string errorLog;
        errorLog.resize(length);
        glGetProgramInfoLog(mProgram, length, &length, &errorLog[0]);
        LOG(errorLog.c_str());
        return;
    }

    glDetachShader(mProgram, mVertexShader->shaderID);
    if (mGeometryShader)
        glDetachShader(mProgram, mGeometryShader->shaderID);
    glDetachShader(mProgram, mFragmentShader->shaderID);

    use();
}

Shader::~Shader()
{
    for (uint8_t i = 0; i < NbShaderTypes; i++)
        if (mShaderFiles[i])
            glDeleteShader(mShaderFiles[i]->shaderID);
    mShaderFiles.fill(nullptr);
    glDeleteProgram(mProgram);
}

int Shader::getUniformBlockLocation(const std::string& block_name) const
{
    return glGetUniformBlockIndex(mProgram, block_name.c_str());
}

int Shader::getUniformLocation(const std::string& uniform_name) const
{
    return glGetUniformLocation(mProgram, uniform_name.c_str());
}

#define SET_UNIFORM_TEMPLATE_SPEC(typename, glFunction)                          \
    template <>                                                                  \
    void Shader::setUniform<typename>(const int& location, typename value) const \
    {                                                                            \
        glFunction(location, value);                                             \
    }
#define SET_UNIFORM_TEMPLATE_SPEC_2_VAR(typename, glFunction)                    \
    template <>                                                                  \
    void Shader::setUniform<typename>(const int& location, typename value) const \
    {                                                                            \
        glFunction(location, value[0], value[1]);                                \
    }
#define SET_UNIFORM_TEMPLATE_SPEC_3_VAR(typename, glFunction)                    \
    template <>                                                                  \
    void Shader::setUniform<typename>(const int& location, typename value) const \
    {                                                                            \
        glFunction(location, value[0], value[1], value[2]);                      \
    }
#define SET_UNIFORM_TEMPLATE_SPEC_4_VAR(typename, glFunction)                    \
    template <>                                                                  \
    void Shader::setUniform<typename>(const int& location, typename value) const \
    {                                                                            \
        glFunction(location, value[0], value[1], value[2], value[3]);            \
    }
#define SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(typename, glFunction, cast_type)         \
    template <>                                                                     \
    void Shader::setUniform<typename>(const int& location, typename value) const    \
    {                                                                               \
        glFunction(location, 1, false, reinterpret_cast<cast_type*>(&value[0][0])); \
    }

#define SET_UNIFORM_TABLE_TEMPLATE_SPEC(typename, glFunction)                                   \
    template <>                                                                                 \
    void Shader::setUniform<typename>(const int& location, typename* value, size_t count) const \
    {                                                                                           \
        glFunction(location, static_cast<int>(count), value);                                   \
    }
#define SET_UNIFORM_TABLE_TEMPLATE_SPEC_C(typename, glFunction, cast_type)                      \
    template <>                                                                                 \
    void Shader::setUniform<typename>(const int& location, typename* value, size_t count) const \
    {                                                                                           \
        glFunction(location, static_cast<int>(count), reinterpret_cast<cast_type*>(value));     \
    }
#define SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(typename, glFunction, cast_type)                         \
    template <>                                                                                           \
    void Shader::setUniform<typename>(const int& location, typename* value, size_t count) const           \
    {                                                                                                     \
        glFunction(location, static_cast<int>(count), false, reinterpret_cast<cast_type*>(&value[0][0])); \
    }

SET_UNIFORM_TEMPLATE_SPEC(int, glUniform1i)
SET_UNIFORM_TEMPLATE_SPEC(unsigned int, glUniform1ui)
SET_UNIFORM_TEMPLATE_SPEC(float, glUniform1f)
SET_UNIFORM_TEMPLATE_SPEC(double, glUniform1d)

SET_UNIFORM_TABLE_TEMPLATE_SPEC(int, glUniform1iv)
SET_UNIFORM_TABLE_TEMPLATE_SPEC(unsigned int, glUniform1uiv)
SET_UNIFORM_TABLE_TEMPLATE_SPEC(float, glUniform1fv)
SET_UNIFORM_TABLE_TEMPLATE_SPEC(double, glUniform1dv)

#ifdef GLM_IMPORTED
SET_UNIFORM_TEMPLATE_SPEC_2_VAR(glm::vec2, glUniform2f)
SET_UNIFORM_TEMPLATE_SPEC_3_VAR(glm::vec3, glUniform3f)
SET_UNIFORM_TEMPLATE_SPEC_4_VAR(glm::vec4, glUniform4f)
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat2, glUniformMatrix2fv, float);
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat2x3, glUniformMatrix2x3fv, float);
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat2x4, glUniformMatrix2x4fv, float);
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat3x2, glUniformMatrix3x2fv, float);
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat3, glUniformMatrix3x2fv, float);
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat3x4, glUniformMatrix3x4fv, float);
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat4x2, glUniformMatrix4x2fv, float);
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat4x3, glUniformMatrix4x3fv, float);
SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C(glm::mat4, glUniformMatrix4fv, float);

SET_UNIFORM_TABLE_TEMPLATE_SPEC_C(glm::vec2, glUniform2fv, float)
SET_UNIFORM_TABLE_TEMPLATE_SPEC_C(glm::vec3, glUniform3fv, float)
SET_UNIFORM_TABLE_TEMPLATE_SPEC_C(glm::vec4, glUniform4fv, float)
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat2, glUniformMatrix2fv, float);
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat2x3, glUniformMatrix2x3fv, float);
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat2x4, glUniformMatrix2x4fv, float);
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat3x2, glUniformMatrix3x2fv, float);
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat3, glUniformMatrix3x2fv, float);
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat3x4, glUniformMatrix3x4fv, float);
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat4x2, glUniformMatrix4x2fv, float);
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat4x3, glUniformMatrix4x3fv, float);
SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C(glm::mat4, glUniformMatrix4fv, float);
#endif

#undef SET_UNIFORM_TEMPLATE_SPEC
#undef SET_UNIFORM_TEMPLATE_SPEC_2_VAR
#undef SET_UNIFORM_TEMPLATE_SPEC_3_VAR
#undef SET_UNIFORM_TEMPLATE_SPEC_4_VAR
#undef SET_UNIFORM_TEMPLATE_SPEC_MATRIX_C
#undef SET_UNIFORM_TABLE_TEMPLATE_SPEC
#undef SET_UNIFORM_TABLE_TEMPLATE_SPEC_C
#undef SET_UNIFORM_TABLE_TEMPLATE_SPEC_MATRIX_C

void Shader::setUniformBlockBinding(const unsigned int& location, const unsigned int& binding) const
{
    glUniformBlockBinding(mProgram, location, binding);
}

void Shader::setUniformBlockBinding(const std::string& block_name, const unsigned int& binding) const
{
    glUniformBlockBinding(mProgram, glGetUniformBlockIndex(mProgram, block_name.c_str()), binding);
}

const unsigned int& Shader::id() const { return mProgram; }

void Shader::use() const { glUseProgram(mProgram); }

std::shared_ptr<Shader::ShaderFile> Shader::__loadShader(ShaderType type, const std::string& filepath)
{
    if (filepath.empty())
        return nullptr;
    std::string code = readContent(filepath);
    if (code.empty())
        return nullptr;
    const char* code_str = code.c_str();

    std::shared_ptr<ShaderFile> shaderFile = std::make_shared<ShaderFile>();
    shaderFile->shaderPath                 = filepath;
    shaderFile->shaderID                   = glCreateShader(shaderTypeToGLType(type));

    if (GL_ERROR(shaderFile->shaderID)) {
        LOG("failed to create shader");
        return nullptr;
    }

    glShaderSource(shaderFile->shaderID, 1, &code_str, nullptr);
    glCompileShader(shaderFile->shaderID);
    assert(glGetError() == GL_NO_ERROR);

    GLint success = 0;
    glGetShaderiv(shaderFile->shaderID, GL_COMPILE_STATUS, &success);
    if (!success) {
        GLint length = 0;
        glGetShaderiv(shaderFile->shaderID, GL_INFO_LOG_LENGTH, &length);
        std::string errorLog;
        errorLog.resize(length);
        glGetShaderInfoLog(shaderFile->shaderID, length, &length, &errorLog[0]);
        std::string s = "Failed to compile shader type: ";
        switch (type) {
            case VertexShader:
                s += "Vertex shader";
                break;
            case GeometryShader:
                s += "Geometry shader";
                break;
            case FragmentShader:
                s += "Fragment shader";
                break;
        }
        LOG(s.c_str());
        LOG(errorLog.c_str());
        glDeleteShader(shaderFile->shaderID);
        return nullptr;
    }

    return shaderFile;
}