#include <array>
#include <cassert>
#include <memory>
#include <tuple>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <IntTypes.h>

#include <rendering/Line.h>
#include <rendering/VAO.h>

Line::Line(): Base()
{
    configure<0>(false, 1, 0);
    enable<0>();
}

Line::~Line() {}

void Line::draw() const
{
    use();
    glDrawArrays(GL_LINE_STRIP, 0, count());
    assert(glGetError() == GL_NO_ERROR);
}

const std::vector<glm::vec2>& Line::points() const { return mLinePoints; }

void Line::points(const std::vector<glm::vec2>& points)
{
    mLinePoints = points;
    reserve<0>(static_cast<unsigned int>(points.size()));
    setData<0>(points.data(), points.size());
}
