#include <cassert>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include <IntTypes.h>

#include <rendering/VBO.h>

template<>
void VBO<float>::configure(
    unsigned int vao, unsigned int index, bool normalized, unsigned int stride, unsigned int offset) const
{
    glVertexArrayVertexBuffer(vao, index, mId, offset * sizeof(float), sizeof(float) * stride);
    glVertexArrayAttribFormat(vao, index, 3, GL_FLOAT, normalized, 0);
    glVertexArrayAttribBinding(vao, index, index);
}

template<>
void VBO<glm::vec2>::configure(
    unsigned int vao, unsigned int index, bool normalized, unsigned int stride, unsigned int offset) const
{
    glVertexArrayVertexBuffer(vao, index, mId, offset * sizeof(glm::vec2), sizeof(glm::vec2) * stride);
    glVertexArrayAttribFormat(vao, index, 2, GL_FLOAT, normalized, 0);
    glVertexArrayAttribBinding(vao, index, index);
}

template<>
void VBO<glm::vec3>::configure(
    unsigned int vao, unsigned int index, bool normalized, unsigned int stride, unsigned int offset) const
{
    glVertexArrayVertexBuffer(vao, index, mId, offset * sizeof(glm::vec3), sizeof(glm::vec3) * stride);
    glVertexArrayAttribFormat(vao, index, 3, GL_FLOAT, normalized, 0);
    glVertexArrayAttribBinding(vao, index, index);
}

template<>
void VBO<glm::vec4>::configure(
    unsigned int vao, unsigned int index, bool normalized, unsigned int stride, unsigned int offset) const
{
    glVertexArrayVertexBuffer(vao, index, mId, offset * sizeof(glm::vec4), sizeof(glm::vec4) * stride);
    glVertexArrayAttribFormat(vao, index, 4, GL_FLOAT, normalized, 0);
    glVertexArrayAttribBinding(vao, index, index);
}
