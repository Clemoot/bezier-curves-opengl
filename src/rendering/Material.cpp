#include <IntTypes.h>

#include <rendering/Material.h>

Material::Material(const std::string& vs, const std::string& fs, const std::string& gs):
    mShader(vs, fs, gs),
    mMVPLocation(mShader.getUniformLocation("uMvp")),
    mViewportLocation(mShader.getUniformLocation("uViewport"))
{
}

Material::~Material() {}

void Material::use() const { mShader.use(); }

void Material::setMVP(const glm::mat4& mvp) const
{
    if (mMVPLocation >= 0)
        mShader.setUniform(mMVPLocation, mvp);
}

void Material::setViewport(unsigned int w, unsigned int h) const
{
    if (mViewportLocation >= 0)
        mShader.setUniform(mViewportLocation, glm::vec2(w, h));
}

const Shader& Material::shader() const { return mShader; }