#include <IntTypes.h>

#include <rendering/Texture.h>

Texture::Texture(unsigned int width, unsigned int height, int internalFormat, int format, int type):
    mWidth(width), mHeight(height), mInternalFormat(internalFormat), mFormat(format), mType(type)
{
    glGenTextures(1, &mTextureID);
    glBindTexture(GL_TEXTURE_2D, mTextureID);

    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, mWidth, mHeight, 0, format, type, nullptr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::Texture(int internalFormat, int format, int type):
    mWidth(800), mHeight(600), mInternalFormat(internalFormat), mFormat(format), mType(type)
{
    glGenTextures(1, &mTextureID);
    glBindTexture(GL_TEXTURE_2D, mTextureID);

    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, mWidth, mHeight, 0, format, type, nullptr);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::~Texture() { glDeleteTextures(1, &mTextureID); }

void Texture::resize(const unsigned int& width, const unsigned int& height)
{
    mWidth  = width;
    mHeight = height;

    glBindTexture(GL_TEXTURE_2D, mTextureID);
    glTexImage2D(GL_TEXTURE_2D, 0, mInternalFormat, mWidth, mHeight, 0, mFormat, mType, nullptr);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::use() const { glBindTexture(GL_TEXTURE_2D, mTextureID); }

const GLuint& Texture::id() const { return mTextureID; }

const GLuint& Texture::height() const { return mHeight; }

const GLuint& Texture::width() const { return mWidth; }
