#include <iostream>
#include <utils/Logging.h>

void __log(const char* file, int line, const char* msg) { std::cout << file << ":" << line << " | " << msg << std::endl; }