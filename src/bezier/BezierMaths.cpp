#include <bezier/BezierMaths.h>

unsigned int BezierMaths::factorial(unsigned int n)
{
    if (n == 0)
        return 1;
    return n * factorial(n - 1);
}

unsigned int BezierMaths::combination(unsigned int k, unsigned int n)
{
    if (k > n)
        return 0;
    return factorial(n) / (factorial(k) * factorial(n - k));
}

int BezierMaths::pow2sign(unsigned int pow) { return 1 - 2 * (pow % 2); }