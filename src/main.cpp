#include <IntTypes.h>

#include <App.h>
#include <iostream>

int main(int, char**)
{
    {
        App app;
        app.run();
    }

    return 0;
}
