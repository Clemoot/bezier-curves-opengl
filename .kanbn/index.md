---
startedColumns:
  - 'In Progress'
completedColumns:
  - Done
---

# BezierCurvesOpenGL

## Backlog

- [implement-bernstein-polynomials](tasks/implement-bernstein-polynomials.md)
- [implement-bezier-curves](tasks/implement-bezier-curves.md)
- [create-curve-sampler](tasks/create-curve-sampler.md)
- [create-line-renderer](tasks/create-line-renderer.md)

## Todo

- [create-basic-rendering-components](tasks/create-basic-rendering-components.md)

## In Progress

## Done

- [create-a-glfw-window](tasks/create-a-glfw-window.md)
- [make-ui-architecture](tasks/make-ui-architecture.md)
