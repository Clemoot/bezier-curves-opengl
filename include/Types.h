#ifndef __TYPES_INCLUDED__
#define __TYPES_INCLUDED__

#include <IntTypes.h>

union Color {
    u8 data[];
    struct {
        u8 r;
        u8 g;
        u8 b;
        u8 a;
    };
};

#endif    // __TYPES_INCLUDED__
