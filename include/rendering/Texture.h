#ifndef __TEXTURE_INCLUDED__
#define __TEXTURE_INCLUDED__

#include <cstdint>
#include <glad/glad.h>

class Texture
{
public:
    Texture(unsigned int width, unsigned int height, int internalFormat, int format, int type);
    Texture(int internalFormat, int format, int type);
    ~Texture();

    void resize(const unsigned int& width, const unsigned int& height);
    void use() const;

    const GLuint& id() const;
    const unsigned int& height() const;
    const unsigned int& width() const;

private:
    GLuint mTextureID;
    unsigned int mWidth, mHeight;
    int mInternalFormat, mFormat, mType;
};

#endif    // __TEXTURE_INCLUDED__
