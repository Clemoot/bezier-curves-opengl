#ifndef __SHADER_INCLUDED__
#define __SHADER_INCLUDED__

#include <array>
#include <cstdint>
#include <glad/glad.h>
#include <memory>
#include <string>

enum ShaderType { VertexShader, GeometryShader, FragmentShader, NbShaderTypes };

class Shader
{
public:
    static const unsigned int shaderTypeToGLType(ShaderType type);

    Shader(const std::string& vsFilepath, const std::string& fsFilepath, const std::string& gsFilepath = "");
    ~Shader();

    int getUniformBlockLocation(const std::string& block_name) const;
    int getUniformLocation(const std::string& uniform_name) const;

    void hotReload();
    const unsigned int& id() const;

    template <typename T>
    void setUniform(const int& location, T value) const;
    template <typename T>
    void setUniform(const int& location, T* data, size_t count) const;
    void setUniformBlockBinding(const unsigned int& location, const unsigned int& binding) const;
    void setUniformBlockBinding(const std::string& block_name, const unsigned int& binding) const;

    void use() const;

private:
    struct ShaderFile {
        std::string shaderPath;
        GLuint shaderID;
    };

    std::array<std::shared_ptr<ShaderFile>, ShaderType::NbShaderTypes> mShaderFiles;

    std::shared_ptr<ShaderFile>& mVertexShader;
    std::shared_ptr<ShaderFile>& mGeometryShader;
    std::shared_ptr<ShaderFile>& mFragmentShader;

    unsigned int mProgram;

    std::shared_ptr<ShaderFile> __loadShader(ShaderType type, const std::string& filepath);
};

#endif    // __SHADER_INCLUDED__
