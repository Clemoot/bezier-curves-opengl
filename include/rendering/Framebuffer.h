#ifndef __FRAMEBUFFER_INCLUDED__
#define __FRAMEBUFFER_INCLUDED__

#include <glad/glad.h>
#include <memory>
#include <rendering/Texture.h>
#include <vector>

enum ColorAttachment {
    Attachment0,
    Attachment1,
    Attachment2,
    Attachment3,
    Attachment4,
    Attachment5,
    Attachment6,
    Attachment7,
    Attachment8,
    Attachment9,
    Attachment10,
    Attachment11,
    Attachment12,
    Attachment13,
    Attachment14,
    Attachment15,
    Attachment16,
    Attachment17,
    Attachment18,
    Attachment19,
    Attachment20,
    Attachment21,
    Attachment22,
    Attachment23,
    Attachment24,
    Attachment25,
    Attachment26,
    Attachment27,
    Attachment28,
    Attachment29,
    Attachment30,
    Attachment31,
    Attachment32,
    NbAttachments
};

class Framebuffer
{
public:
    Framebuffer();
    ~Framebuffer();

    void bind() const;
    void createColorAttachment(ColorAttachment attachment);
    void createDepthStencilAttachment();
    const bool isValid() const;
    static void unbind();

    const std::shared_ptr<Texture>& attachment(ColorAttachment attachment) const;
    const std::shared_ptr<Texture>& depthAttachment() const;
    const GLuint& id() const;

private:
    struct TextureAttachment {
        std::shared_ptr<Texture> texture;
        unsigned int attachment;

        TextureAttachment(std::shared_ptr<Texture> texture, unsigned int attachment);
    };

    GLuint mFramebufferID;
    std::vector<TextureAttachment> mAttachments;
    std::shared_ptr<Texture> mDepthStencilAttachment;
};

#endif    // __FRAMEBUFFER_INCLUDED__
