#ifndef __MATERIAL_INCLUDED__
#define __MATERIAL_INCLUDED__

#include <glm/glm.hpp>
#include <rendering/Shader.h>
#include <utils/FixedString.h>

class Material
{
public:
    Material(const std::string& vs, const std::string& fs, const std::string& gs = "");
    ~Material();

    virtual void use() const;

    void setMVP(const glm::mat4& mvp) const;
    void setViewport(unsigned int w, unsigned int h) const;

    const Shader& shader() const;

protected:
    Shader mShader;

private:
    int mMVPLocation;
    int mViewportLocation;
};

#endif    // __MATERIAL_INCLUDED__
