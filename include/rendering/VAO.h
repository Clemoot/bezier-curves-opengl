#ifndef __VAO_INCLUDED__
#define __VAO_INCLUDED__

#include <rendering/VBO.h>

template <typename T>
class Buffer;

class IVAO
{
protected:
    static constexpr unsigned int INVALID_VAO = std::numeric_limits<unsigned int>::max();
};

template <typename... Types>
class VAO : public IVAO
{
    static constexpr unsigned int INVALID_BINDING = std::numeric_limits<unsigned int>::max();

    using VBOs_t = std::tuple<std::unique_ptr<VBO<Types>>...>;
    template <unsigned int I>
    using VBO_t = std::tuple_element_t<I, VBOs_t>;

    static constexpr unsigned int NbVBOs = std::tuple_size_v<VBOs_t>;

public:
    template <bool ManualInstantiation = false>
    VAO();
    VAO(const VAO& other) = delete;
    VAO(VAO&& other);
    ~VAO();

    template <unsigned int I>
    void configure(bool normalized, unsigned int stride, unsigned int offset, unsigned int binding = I);

    template <unsigned int I>
    void disable() const;

    template <unsigned int I>
    void enable() const;

    template <unsigned int I>
    const VBO_t<I>& getVBO() const;

    template <unsigned int I, typename... ReserveArgs>
    void reserve(ReserveArgs&&... args);

    template <unsigned int I, typename... SetDataArgs>
    void setData(SetDataArgs&&... args) const;

    void use() const;

    unsigned int id() const { return mId; }
    unsigned int count() const { return mCount; }

private:
    unsigned int mId;

    VBOs_t mVBOs;
    std::array<unsigned int, NbVBOs> mBindings;

    unsigned int mCount;

    template <bool ManualInstantiation>
    VBOs_t instantiateBuffers();
};

template <typename... Types>
template <bool ManualInstantiation>
VAO<Types...>::VAO(): mVBOs(instantiateBuffers<ManualInstantiation>()), mBindings{ INVALID_BINDING }, mCount(0)
{
    glCreateVertexArrays(1, &mId);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename... Types>
VAO<Types...>::~VAO()
{
    if (mId != INVALID_VAO) {
        glDeleteVertexArrays(1, &mId);
        assert(glGetError() == GL_NO_ERROR);
    }
}

template <typename... Types>
VAO<Types...>::VAO(VAO&& other):
    mId(other.mId), mVBOs(std::move(other.mVBOs)), mBindings(std::move(other.mBindings)), mCount(other.mCount)
{
    other.mId = INVALID_VAO;
}

template <typename... Types>
template <unsigned int I>
void VAO<Types...>::configure(bool normalized, unsigned int stride, unsigned int offset, unsigned int binding)
{
    const VBO_t<I>& vbo = getVBO<I>();
    assert(vbo);
    assert(binding < GL_MAX_VERTEX_ATTRIB_BINDINGS);
    vbo->configure(mId, binding, normalized, stride, offset);
    mBindings[I] = binding;
}

template <typename... Types>
template <unsigned int I>
void VAO<Types...>::disable() const
{
    static_assert(I < NbVBOs);
    assert(mBindings[I] != INVALID_BINDING);
    glDisableVertexArrayAttrib(mId, mBindings[I]);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename... Types>
template <unsigned int I>
void VAO<Types...>::enable() const
{
    static_assert(I < NbVBOs);
    assert(mBindings[I] != INVALID_BINDING);
    glEnableVertexArrayAttrib(mId, mBindings[I]);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename... Types>
template <unsigned int I>
const VAO<Types...>::VBO_t<I>& VAO<Types...>::getVBO() const
{
    static_assert(I < NbVBOs);
    return std::get<I>(mVBOs);
}

template <typename... Types>
template <unsigned int I, typename... ReserveArgs>
void VAO<Types...>::reserve(ReserveArgs&&... args)
{
    static_assert(I < NbVBOs);
    const VBO_t<I>& vbo = getVBO<I>();
    assert(vbo);
    vbo->reserve(args...);
    assert(glGetError() == GL_NO_ERROR);
    if (vbo->count() > mCount)
        mCount = vbo->count();
}

template <typename... Types>
template <unsigned int I, typename... SetDataArgs>
void VAO<Types...>::setData(SetDataArgs&&... args) const
{
    const VBO_t<I>& vbo = getVBO<I>();
    assert(vbo);
    vbo->setData(args...);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename... Types>
void VAO<Types...>::use() const
{
    assert(mId != INVALID_VAO);
    glBindVertexArray(mId);
}

template <typename... Types>
template <bool ManualInstantiation>
VAO<Types...>::VBOs_t VAO<Types...>::instantiateBuffers()
{
    if constexpr (!ManualInstantiation) {
        return std::make_tuple<std::unique_ptr<VBO<Types>>...>(std::make_unique<VBO<Types>>()...);
    } else {
        return std::make_tuple<std::unique_ptr<VBO<Types>>...>(nullptr);
    }
}

#endif    // __VAO_INCLUDED__