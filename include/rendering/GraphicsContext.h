#ifndef __GRAPHICS_CONTEXT_INCLUDED__
#define __GRAPHICS_CONTEXT_INCLUDED__

class GraphicsContext
{
  public:
    GraphicsContext();
    virtual ~GraphicsContext();
};

#endif    // __GRAPHICS_CONTEXT_INCLUDED__
