#ifndef __LINE_RENDERER_MATERIAL_INCLUDED__
#define __LINE_RENDERER_MATERIAL_INCLUDED__

#include <rendering/Material.h>

class LineRendererMaterial : public Material
{
  public:
    LineRendererMaterial();
    ~LineRendererMaterial();

  private:
    int mViewportSize;
};

#endif    // __LINE_RENDERER_MATERIAL_INCLUDED__
