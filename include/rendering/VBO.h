#ifndef __VBO_INCLUDED__
#define __VBO_INCLUDED__

class IVBO
{
protected:
    static constexpr unsigned int INVALID_VBO = std::numeric_limits<unsigned int>::max();
};

template <typename T>
class VBO : public IVBO
{
public:
    VBO();
    VBO(const T& data);
    VBO(const T* data, unsigned int count);
    VBO(const VBO& other) = delete;
    VBO(VBO&& other);
    ~VBO();

    void configure(unsigned int vao, unsigned int index, bool normalized, unsigned int stride, unsigned int offset) const;

    void reserve(unsigned int count);

    void setData(const T& data);
    void setData(const T* data, unsigned int count);
    void setData(const T& data, unsigned int offset);
    void setData(const T* data, unsigned int offset, unsigned int count);

    void use(unsigned int target) const;

    unsigned int count() const { return mCount; }
    unsigned int offset() const { return 0; }
    unsigned int size() const { return mSize; }

    unsigned int id() const { return mId; }
    bool valid() const { return mId != INVALID_VBO; }

private:
    unsigned int mId;
    unsigned int mCount;
    unsigned int mSize;

    void __allocateBuffer(const T* data);
};

template <typename T>
VBO<T>::VBO(): mId(INVALID_VBO), mCount(0), mSize(0)
{
    __allocateBuffer(nullptr);
}

template <typename T>
VBO<T>::VBO(const T& data): mId(INVALID_VBO), mCount(1), mSize(sizeof(T))
{
    __allocateBuffer(&data);
}

template <typename T>
VBO<T>::VBO(const T* data, unsigned int count): mId(INVALID_VBO), mCount(count), mSize(sizeof(T) * count)
{
    __allocateBuffer(data);
}

template <typename T>
VBO<T>::VBO(VBO&& other): mId(other.mId), mCount(other.mCount), mSize(other.mSize)
{
    other.mId = INVALID_VBO;
}

template <typename T>
VBO<T>::~VBO()
{
    if (mId != INVALID_VBO) {
        glDeleteBuffers(1, &mId);
        assert(glGetError() == GL_NO_ERROR);
    }
}

template <typename T>
void VBO<T>::reserve(unsigned int count)
{
    const unsigned int newSize = sizeof(T) * count;
    if (newSize > mSize) {
        mCount = count;
        mSize  = newSize;

        glNamedBufferData(mId, mSize, nullptr, GL_STATIC_DRAW);
        assert(glGetError() == GL_NO_ERROR);
    }
}

template <typename T>
void VBO<T>::setData(const T& data)
{
    glNamedBufferSubData(mId, 0, sizeof(T), &data);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename T>
void VBO<T>::setData(const T* data, unsigned int count)
{
    const unsigned int size = sizeof(T) * count;
    assert(size <= mSize);
    glNamedBufferSubData(mId, 0, size, data);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename T>
void VBO<T>::setData(const T& data, unsigned int offset)
{
    assert(offset < mSize);
    glNamedBufferSubData(mId, offset, sizeof(T), &data);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename T>
void VBO<T>::setData(const T* data, unsigned int offset, unsigned int count)
{
    const unsigned int size = sizeof(T) * count;
    assert(offset + size <= mSize);
    glNamedBufferSubData(mId, offset, size, data);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename T>
void VBO<T>::use(unsigned int target) const
{
    glBindBuffer(target, mId);
    assert(glGetError() == GL_NO_ERROR);
}

template <typename T>
void VBO<T>::__allocateBuffer(const T* data)
{
    glCreateBuffers(1, &mId);
    assert(glGetError() == GL_NO_ERROR);
    if (mSize > 0)
        glNamedBufferData(mId, mSize, data, GL_STATIC_DRAW);
    assert(glGetError() == GL_NO_ERROR);
}

#endif    // __BUFFER_INCLUDED__
