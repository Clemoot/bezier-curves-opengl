#ifndef __LINE_MATERIAL_INCLUDED__
#define __LINE_MATERIAL_INCLUDED__

#include <rendering/Material.h>

class LineMaterial : public Material
{
public:
    LineMaterial();
    ~LineMaterial();

    float lineWidth() const;
    void lineWidth(float lineWidth);

    void blendFactor(float blendFactor);

private:
    int mLineWidthLocation;
    int mBlendFactorLocation;
    float mLineWidth;
};

#endif    // __LINE_MATERIAL_INCLUDED__
