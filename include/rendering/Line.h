#ifndef __LINE_INCLUDED__
#define __LINE_INCLUDED__

#include <vector>

#include <rendering/VAO.h>

class Line : protected VAO<glm::vec2>
{
    using Base = VAO<glm::vec2>;

public:
    Line();
    ~Line();

    void use() const { Base::use(); }

    void draw() const;

    const std::vector<glm::vec2>& points() const;

    void points(const std::vector<glm::vec2>& points);

private:
    std::vector<glm::vec2> mLinePoints;
};

#endif    // __LINE_INCLUDED__
