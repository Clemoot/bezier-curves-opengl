#ifndef __GLFW_WINDOW_INCLUDED__
#define __GLFW_WINDOW_INCLUDED__

#define GLFW_INCLUDE_NONE
#include "Window.h"
#include <GLFW/glfw3.h>
#include <glad/glad.h>

class GLFWWindow : public Window
{
    static uint8_t smNbGLFWWindows;
    static bool    smContextInitialized;
    static void    __initializeContext();

  public:
    GLFWWindow(const std::string& name, const uint16_t& width, const uint16_t& height);
    ~GLFWWindow();

    const bool isClosed() const override;
    void       hide() override;
    void       pollEvents() const override;
    void       show() override;
    void       swapBuffers() const override;

  private:
};

#endif    // __GLFW_WINDOW_INCLUDED__
