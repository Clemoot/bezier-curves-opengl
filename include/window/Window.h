#ifndef __WINDOW_INCLUDED__
#define __WINDOW_INCLUDED__

#include <cstdint>
#include <string>

class Window
{
  public:
    Window(const std::string& name, const uint16_t& width, const uint16_t& height);
    virtual ~Window();

    virtual void       hide()              = 0;
    virtual const bool isClosed() const    = 0;
    virtual void       pollEvents() const  = 0;
    virtual void       show()              = 0;
    virtual void       swapBuffers() const = 0;

    const std::string& name() const;
    void* const&       handle() const;
    const uint16_t&    height() const;
    const uint16_t&    width() const;

  protected:
    void* mHandle;

    std::string mName;
    uint16_t    mWidth;
    uint16_t    mHeight;

    bool mIsClosed;
};

#endif    // __WINDOW_INCLUDED__
