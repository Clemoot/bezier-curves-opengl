#ifndef __UI_CONTEXT_INCLUDED__
#define __UI_CONTEXT_INCLUDED__

#include <window/Window.h>

class UIContext
{
  public:
    UIContext(Window& window, bool enableDockspace = false);
    virtual ~UIContext();

    void startFrame() const;
    void renderFrame() const;
};

#endif    // __UI_CONTEXT_INCLUDED__
