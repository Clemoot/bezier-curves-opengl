#ifndef __CONFIGURABLE_UI_WINDOW__
#define __CONFIGURABLE_UI_WINDOW__

#include <cstdint>
#include <memory>
#include <ui/UIWindow.h>
#include <vector>

class ConfigurableUIWindow : public UIWindow
{
  public:
    ConfigurableUIWindow(const std::string& windowName, const ImGuiWindowFlags& flags = 0);
    virtual ~ConfigurableUIWindow();

    virtual void displayOptions() = 0;

  private:
};

#endif    // __CONFIGURABLE_UI_WINDOW__
