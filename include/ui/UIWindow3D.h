#ifndef __3D_UI_WINDOW_INCLUDED__
#define __3D_UI_WINDOW_INCLUDED__

#include <cstdint>
#include <rendering/Framebuffer.h>
#include <rendering/Line.h>
#include <rendering/LineMaterial.h>
#include <rendering/VBO.h>
#include <ui/ConfigurableUIWindow.h>

#include <bezier/BernsteinPolynomials.h>
#include <bezier/Sampler.h>

class UIWindow3D : public ConfigurableUIWindow
{
public:
    UIWindow3D();
    ~UIWindow3D();

    virtual void displayOptions();

protected:
    virtual void __displayWindowContent();

private:
    float mPan;
    static unsigned int smNbUIWindow3Ds;

    Framebuffer mFramebuffer;
    Line mLine;
    LineMaterial mLineMaterial;

    std::vector<glm::vec2> mAnchors;
    BernsteinPolynomials<float, int> mPolynomials;
    Sampler<decltype(mPolynomials)> mSampler;

    void __computeLine();
};

#endif    // __3D_UI_WINDOW_INCLUDED__
