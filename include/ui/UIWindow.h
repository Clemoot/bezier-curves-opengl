#ifndef __UI_WINDOW_INCLUDED__
#define __UI_WINDOW_INCLUDED__

#include <imgui.h>
#include <string>

class UIWindow
{
  public:
    UIWindow(const std::string& windowName, const ImGuiWindowFlags& flags = 0);
    virtual ~UIWindow();

    virtual void displayWindow();

  protected:
    bool             mOpened;
    ImGuiWindowFlags mWindowFlags;
    std::string      mWindowName;

    virtual void __displayWindowContent() = 0;
};

#endif    // __UI_WINDOW_INCLUDED__
