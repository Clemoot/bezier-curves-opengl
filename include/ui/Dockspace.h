#ifndef __DOCKSPACE_INCLUDED__
#define __DOCKSPACE_INCLUDED__

#include <cstdint>
#include <memory>
#include <ui/UIWindow.h>
#include <vector>

class Dockspace : public UIWindow
{
public:
    Dockspace();
    ~Dockspace();

    void addWindow(std::shared_ptr<UIWindow> window);
    virtual void displayWindow();

protected:
    virtual void __displayWindowContent();

private:
    static unsigned int smNbDockspaces;

    std::vector<std::shared_ptr<UIWindow>> mChildWindows;
    ImGuiID mDockspaceID;
};

#endif    // __DOCKSPACE_UI_WINDOW_INCLUDED__
