#ifndef __CONFIG_UI_WINDOW_INCLUDED__
#define __CONFIG_UI_WINDOW_INCLUDED__

#include <cstdint>
#include <memory>
#include <ui/ConfigurableUIWindow.h>
#include <vector>

class ConfigUIWindow : public UIWindow
{
public:
    ConfigUIWindow();
    ~ConfigUIWindow();

    void addConfigurableWindow(std::shared_ptr<ConfigurableUIWindow> window);

protected:
    virtual void __displayWindowContent();

private:
    static unsigned int smNbConfigUIWindows;

    std::vector<std::shared_ptr<ConfigurableUIWindow>> mConfigurableWindows;
};

#endif    // __CONFIG_UI_WINDOW_INCLUDED__
