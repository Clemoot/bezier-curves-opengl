#ifndef __SAMPLER_INCLUDED__
#define __SAMPLER_INCLUDED__

#include <concepts>

template <class T>
concept Samplable = requires(T a, float b)
{
    { a(b) };
};

template <Samplable S>
class Sampler
{
public:
    using ValueType = decltype(S().operator()(std::declval<float>()));
    using PairType  = std::pair<float, typename ValueType>;

    Sampler(const S& sampleTarget);
    ~Sampler();

    const float& inputMin() const;
    const float& inputMax() const;
    const unsigned int& sampleCount() const;

    Sampler<S>& inputMin(const float& min);
    Sampler<S>& inputMax(const float& max);
    Sampler<S>& sampleCount(const unsigned int& sampleCount);

    std::vector<typename Sampler<S>::PairType> sample() const;

private:
    float mInputMin;
    float mInputMax;
    unsigned int mSampleCount;
    const S& mSampleTarget;
};

template <Samplable S>
Sampler<S>::Sampler(const S& sampleTarget): mInputMin(0.f), mInputMax(0.f), mSampleCount(0), mSampleTarget(sampleTarget)
{
}

template <Samplable S>
Sampler<S>::~Sampler()
{
}

template <Samplable S>
const float& Sampler<S>::inputMin() const
{
    return mInputMin;
}

template <Samplable S>
const float& Sampler<S>::inputMax() const
{
    return mInputMax;
}

template <Samplable S>
const unsigned int& Sampler<S>::sampleCount() const
{
    return mSampleCount;
}

template <Samplable S>
Sampler<S>& Sampler<S>::inputMin(const float& min)
{
    mInputMin = min;
    return *this;
}

template <Samplable S>
Sampler<S>& Sampler<S>::inputMax(const float& max)
{
    mInputMax = max;
    return *this;
}

template <Samplable S>
Sampler<S>& Sampler<S>::sampleCount(const unsigned int& sampleCount)
{
    mSampleCount = sampleCount;
    return *this;
}

template <Samplable S>
std::vector<typename Sampler<S>::PairType> Sampler<S>::sample() const
{
    if (mSampleCount == 0)
        return {};

    std::vector<typename Sampler<S>::PairType> res;
    res.reserve(mSampleCount);

    if (mSampleCount == 1) {
        res.emplace_back(mInputMin, mSampleTarget(mInputMin));
        return res;
    }

    float start = mInputMin;
    float step  = (mInputMax - mInputMin) / (mSampleCount - 1);
    for (unsigned int i = 0; i < mSampleCount; i++, start += step)
        res.emplace_back(start, mSampleTarget(start));

    return res;
}

#endif    // __SAMPLER_INCLUDED__
