#ifndef __BEZIER_MATHS_INCLUDED__
#define __BEZIER_MATHS_INCLUDED__

namespace BezierMaths
{
unsigned int factorial(unsigned int n);
unsigned int combination(unsigned int k, unsigned int n);
int pow2sign(unsigned int pow);
}    // namespace BezierMaths

#endif    // __BEZIER_MATHS_INCLUDED__
