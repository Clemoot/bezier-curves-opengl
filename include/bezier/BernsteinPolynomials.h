#ifndef __BERNSTEIN_POLYNOMIALS_INCLUDED__
#define __BERNSTEIN_POLYNOMIALS_INCLUDED__

#include <vector>

#include <bezier/BezierMaths.h>

template <typename InputType = float, typename CoefType = InputType>
class BernsteinPolynomials
{
public:
    BernsteinPolynomials();
    BernsteinPolynomials(size_t degree);
    ~BernsteinPolynomials() = default;

    size_t getDegree() const;
    // Set the degree of embedded polynomials (and recompute coefficients)
    void setDegree(size_t degree);

    std::vector<InputType> operator()(InputType t) const;

private:
    std::vector<std::vector<CoefType>> mPolynomials;

    // Compute polynomials coefficents
    void computeCoefficients();
};

template <typename InputType, typename CoefType>
BernsteinPolynomials<InputType, CoefType>::BernsteinPolynomials()
{
}

template <typename InputType, typename CoefType>
BernsteinPolynomials<InputType, CoefType>::BernsteinPolynomials(size_t degree)
{
    setDegree(degree);
}

template <typename InputType, typename CoefType>
size_t BernsteinPolynomials<InputType, CoefType>::getDegree() const
{
    return mPolynomials.size() - 1;
}

template <typename InputType, typename CoefType>
void BernsteinPolynomials<InputType, CoefType>::setDegree(size_t degree)
{
    mPolynomials.resize(degree + 1);
    for (std::vector<CoefType>& poly : mPolynomials)
        poly.resize(degree + 1, {});
    computeCoefficients();
}

template <typename InputType, typename CoefType>
std::vector<InputType> BernsteinPolynomials<InputType, CoefType>::operator()(InputType t) const
{
    if (mPolynomials.empty())
        return {};

    size_t nbCoefs = getDegree() + 1;
    // Fill input vector
    std::vector<InputType> input(nbCoefs, 1.f);
    for (size_t i = 1; i < nbCoefs; i++)
        input[i] = input[i - 1] * t;

    // Matrix multiplication
    std::vector<InputType> res(nbCoefs, 0.f);
    for (size_t i = 0; i < nbCoefs; i++) {
        const std::vector<CoefType>& poly = mPolynomials[i];
        assert(poly.size() == nbCoefs);

        for (size_t j = 0; j < nbCoefs; j++)
            res[i] += poly[j] * input[j];
    }

    return res;
}

template <typename InputType, typename CoefType>
void BernsteinPolynomials<InputType, CoefType>::computeCoefficients()
{
    size_t degree = getDegree();
    for (unsigned int i = 0; i <= degree; i++)
        for (unsigned int k = 0, kmax = degree - i; k <= kmax; k++)
            mPolynomials[i][i + k] =
                static_cast<int>(BezierMaths::combination(k, degree - i) * BezierMaths::combination(i, degree)) *
                BezierMaths::pow2sign(k);
}

#endif    // __BERNSTEIN_POLYNOMIALS_INCLUDED__
