#ifndef __FIXED_STRING_INCLUDED__
#define __FIXED_STRING_INCLUDED__

#include <algorithm>

template<size_t N>
struct FixedString {
    char str[N];
    constexpr FixedString(const char (&str_)[N]): str{ 0 }
    {
        for (size_t i = 0; i < N; i++)
            str[i] = str_[i];
    }
};

#endif    // __FIXED_STRING_INCLUDED__
