#ifndef __LOGGING_INCLUDED__
#define __LOGGING_INCLUDED__

#define LOG(msg) __log(__FILE__, __LINE__, msg)
#define ENABLE_GLFW_ERROR_LOG_FUNC \
    void logGLFWError(int code, const char* description) { LOG(description); }

void __log(const char* file, int line, const char* msg);

#endif    // __LOGGING_INCLUDED__