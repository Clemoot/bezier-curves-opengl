#ifndef __UTILS_INCLUDED__
#define __UTILS_INCLUDED__

#define GL_ERROR(res)             \
    unsigned int errorCode = res; \
    !errorCode

#endif    // __UTILS_INCLUDED__
