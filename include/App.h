#ifndef __APP_INCLUDED__
#define __APP_INCLUDED__

#include <bezier/BernsteinPolynomials.h>
#include <iostream>
#include <memory>
#include <rendering/Framebuffer.h>
#include <rendering/GraphicsContext.h>
#include <rendering/Material.h>
#include <rendering/Shader.h>
#include <rendering/VBO.h>
#include <ui/ConfigUIWindow.h>
#include <ui/Dockspace.h>
#include <ui/UIContext.h>
#include <ui/UIWindow3D.h>
#include <utils/Logging.h>
#include <window/GLFWWindow.h>

class App
{
  public:
    App();
    ~App();

    void run();

  private:
    GLFWWindow                      mWindow;
    GraphicsContext                 mGraphicsContext;
    UIContext                       mUIContext;
    std::unique_ptr<Dockspace>      mDockspace;
    std::shared_ptr<ConfigUIWindow> mConfigWindow;
    std::shared_ptr<UIWindow3D>     m3DWindow;
};

#endif    // __APP_INCLUDED__
