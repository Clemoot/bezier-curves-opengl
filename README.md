# Bezier Curves with OpenGL

By reading [a book explaining how are made curves](https://www.editions-ellipses.fr/accueil/3363-modeles-de-bezier-des-b-splines-et-des-nurbs-mathematiques-des-courbes-et-des-surfaces-9782729898069.html), I wanted to implement what I learned through this project. This goal is to visualize the construction of Bezier curves and edit its parameters. It is made with open-source and cross-platform libraries, so it should be compatible on all platforms.

## Build

This project is made with a classic CMake configuration. You simply need to configure and build with the correct kit.