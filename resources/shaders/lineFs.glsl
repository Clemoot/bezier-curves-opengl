#version 400

in vec2 fLineCenter;
in vec2 lineWidth;

uniform vec2 uViewport;
uniform float uLineWidth;
uniform float uBlendFactor;

out vec4 frag;

void main()
{
    vec4 c          = vec4(1.0, 0.0, 0.0, 1.0);
    vec2 lineCenter = fLineCenter * uViewport;
    double d        = length(lineCenter - gl_FragCoord.xy);
    if (d > uLineWidth)
        c.w = 0;
    else
        // c.w *= pow(float((uLineWidth - d) / uLineWidth), uBlendFactor);
        c.w *= smoothstep(float((uLineWidth - d) / uLineWidth), 0.0, uBlendFactor);
    frag = c;
}