#version 420

layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;

in VS_OUT { vec2 vLineCenter; }
gs_in[];

uniform mat4 uMvp;

out vec2 fLineCenter;

void main()
{
    vec4 start = gl_in[0].gl_Position;
    vec4 end   = gl_in[1].gl_Position;

    vec3 dir  = (end - start).xyz;
    vec4 rDir = vec4(normalize(cross(dir, vec3(0.0, 0.0, 1.0))), 0.0);
    // Make the added edges follow the screen stretch
    rDir *= uMvp;

    gl_Position = start - rDir;
    fLineCenter = gs_in[0].vLineCenter;
    EmitVertex();

    gl_Position = start + rDir;
    fLineCenter = gs_in[0].vLineCenter;
    EmitVertex();

    gl_Position = end - rDir;
    fLineCenter = gs_in[1].vLineCenter;
    EmitVertex();

    gl_Position = end + rDir;
    fLineCenter = gs_in[1].vLineCenter;
    EmitVertex();

    EndPrimitive();
}