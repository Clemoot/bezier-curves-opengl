#version 420

in vec2 v;

out VS_OUT { vec2 vLineCenter; }
vs_out;

void main()
{
    gl_Position        = vec4(v, 0.0, 1.0);
    vs_out.vLineCenter = 0.5 * (v + vec2(1, 1));
}